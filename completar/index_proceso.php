<?php
function validaIngreso($parametro)
{
	// Funcion utilizada para validar el dato a ingresar recibido por GET
	$parametro=trim($parametro);
	
	if(preg_match("^[a-zA-Z0-9.@ ]{4,40}$", $parametro)) return TRUE;
	else return FALSE;
}

function validaBusqueda($parametro)
{
	// Funcion para validar la cadena de busqueda de la lista desplegable
	if(preg_match("^[a-zA-Z0-9.@ ]{2,40}$", $parametro)) return TRUE;
	else return FALSE;
}

if(isset($_POST["ingreso"]))
{
	$valor=$_POST["ingreso"];
	if(validaIngreso($valor))
	{
		$coneccion=mysqli_connect($servidor,$usuario,$password) or die(mysqli_error($coneccion));
		mysqli_select_db($coneccion,"shopping_merck") or die(mysqli_error($coneccion));
		
		$consulta=mysqli_query($coneccion,"SELECT COUNT(*) FROM productos") or die(mysqli_error($coneccion));
		$registro=mysqli_fetch_row($consulta);

		if($registro[0]>=1000) { echo "Hay demasiados registros en la Base de Datos"; die(); }
		
		$consulta=mysqli_query($coneccion,"SELECT * FROM productos WHERE PRODUCTOS='$valor'") or die(mysqli_error($coneccion));
		$registro=mysqli_fetch_row($consulta);
		
		if($registro) { echo "Ya existe tu nombre en la Base de Datos"; }
		else
		{
			mysqli_query($coneccion,"INSERT into productos (PRODUCTOS) VALUES ('$valor')");
			echo "Tu nombre ha sido ingresado";
		}
		mysqli_close($coneccion);
	}
}
if(isset($_POST["busqueda"]))
{
	$valor=$_POST["busqueda"];
	if(validaBusqueda($valor))
	{
		
		$coneccion=mysqli_connect($servidor,$usuario,$password) or die(mysqli_error($coneccion));
		mysqli_select_db($coneccion,"shopping_merck") or die(mysqli_error($coneccion));
		
		//$consulta=mysql_query("SELECT nombre FROM autocompletador WHERE MATCH(nombre) AGAINST('".$valor."*' IN BOOLEAN MODE) LIMIT 0, 22") or die(mysql_error());
		$consulta=mysqli_query($coneccion,"SELECT PRODUCTOS FROM productos WHERE PRODUCTOS LIKE '".$valor."%' LIMIT 0, 22");
		
		mysqli_close($coneccion);
		
		$cantidad=mysqli_num_rows($consulta);
		if($cantidad==0)
		{
			/* 0: no se vuelve por mas resultados
			vacio: cadena a mostrar, en este caso no se muestra nada */
			echo "0&vacio";
		}
		else
		{
			if($cantidad>20) echo "1&"; 
			else echo "0&";
	
			$cantidad=1;
			while(($registro=mysqli_fetch_row($consulta)) && $cantidad<=20)
			{
				echo "<div onClick=\"clickLista(this);\" onMouseOver=\"mouseDentro(this);\">".$registro[0]."</div>";
				// Muestro solo 20 resultados de los 22 obtenidos
				$cantidad++;
			}
		}
	}
}
?>